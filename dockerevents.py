#!/usr/bin/env python

import subprocess

from docker.client import Client
import json

def load_settings(filename="settings.json"):
    with open(filename) as settings_file:
        try:
            settings = json.load(settings_file)
        except (ValueError, Exception) as e:
            print("Incorrect configuration file")
            settings = None
        return settings


def exec_cmd(cmd, args):
    if cmd is None:
        return 0
    try:
        subprocess.Popen([cmd, args])
    except Exception:
        return 0


def main():
    settings = load_settings()
    if settings == None:
        print("Exit")
        return 0
    else:
        client = Client()
        events = client.events()
        for event in events:
            event_str = event.decode("utf-8")
            event_dict = json.loads(event_str)
            event_type = event_dict['status']
            if event_type == 'create':
                try:
                    exec_cmd(settings['on_create']['cmd'], settings['on_create']['args'])
                except Exception:
                    pass
            if event_type == 'start':
                try:
                    exec_cmd(settings['on_start']['cmd'], settings['on_start']['args'])
                except Exception:
                    pass
            if event_type == 'stop':
                try:
                    exec_cmd(settings['on_stop']['cmd'], settings['on_stop']['args'])
                except Exception:
                    pass
            if event_type == 'destroy':
                try:
                    exec_cmd(settings['on_destroy']['cmd'], settings['on_destroy']['args'])
                except Exception:
                    pass
            if event_type == 'restart':
                try:
                    exec_cmd(settings['on_restart']['cmd'], settings['on_restart']['args'])
                except Exception:
                    pass
            if event_type == 'kill':
                try:
                    exec_cmd(settings['on_kill']['cmd'], settings['on_kill']['args'])
                except Exception:
                    pass
            if event_type == 'die':
                try:
                    exec_cmd(settings['on_die']['cmd'], settings['on_die']['args'])
                except Exception:
                    pass


if __name__ == "__main__":
    main()
