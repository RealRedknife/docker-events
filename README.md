## Установка зависимостей ##

```
pip install -r requirements.txt
```


## Настройки ##

Файл настроек settings.json должен находиться в той же директории, что и dockerevents.py
```
#!json
"on_create": {
    "cmd": "/usr/local/bin/testscript",
    "args": "arg"
  },
```
После создания любого контейнера, будет исполнен 
```
/usr/local/bin/testscript arg
```



## Запуск ##

```
chmod u+x dockerevents.py
./dockerevents.py &
```